% convert data to txt format for use in R

clear;

load ../software/simu_data;
save -ascii simu_data.txt f;

load ../software/toy_data;
save -ascii toy_data.txt f;

load ../software/CurvReg3ieg11ToyDataSet;
save -ascii CurvReg3ieg11ToyDataSet.txt f;

load ../software/wave_data;
save -ascii wave_data.txt f;

load ../software/growth_male_vel;
save -ascii growth_male_vel.txt f;

load ../software/growth_female_vel;
save -ascii growth_female_vel.txt f;

load ../software/simple_data;
f = x';
save -ascii simple_data.txt f;

load ../software/signa_data;
save -ascii signa_data.txt f;

load ../software/neural_data;
save -ascii neural_data.txt f;

load ../software/gene_data; t = t'; f = f(l==1,:)';
save -ascii gene_data.txt f;

