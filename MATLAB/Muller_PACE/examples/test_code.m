% test_code


clear; close all;

%load ../../software/growth_male_vel;
%load ../../software/growth_female_vel;
%load  ../../software/toy_data;
%load ../../software/simu_data;
%load ../../software/wave_data;
%load ../../software/CurvReg3ieg10ToyDataSet;
%load ../../software/CurvReg3ieg11ToyDataSet;
%load ../../software/simu_data2_1;
%load ../../software/simple_data;  f = x'; t = t';
load ../../software/signa_data;
%load ../../software/neural_data;
%load ../../software/gene_data; t = t'; f = f(l==1,:)';

T = (t'-t(1))/(t(end)-t(1));
Y = f'; 
[N, M] = size(Y); 

tic;
%%%%%%%%%%%%%%%% use WFPCA function %%%%%%%%%%%%%%%%%%%%%%%%%%%
d = path;
isExist = regexp(d, 'PACE');
if isempty(isExist) == 1
addpath(genpath('../PACE/'));
end
[X,aligned,h,hinv]=WFPCA(Y,T);

toc,

% plot and compute goodness of alignment
f = Y;
fn = aligned;

figure(1);
set(gcf, 'position', [200 500 250 200]);
plot(t, fn);
%axis([t(1) t(end) -1.2 1.2]);  % wave data
%axis([t(1) t(end) 0 1.2]);  % extreme data
%axis([t(1) t(end) 0 9]);  % toy data
%axis([t(1) t(end) 0 1.5]);  % signa data, nueral data, simu data
%axis([t(1) t(end) -5 35]);  % growth male data
axis([t(1) t(end) -2.5 3]);  % gene data

% measure the level of synchronization (James, 2011)
for i = 1:N
    ind = [1:i-1 i+1:N];
    sync(i) = sum((fn(i,:)-mean(fn(ind,:),1)).^2)/sum((f(i,:)-mean(f(ind,:),1)).^2);
end
disp(sprintf('sync = %5.4f', mean(sync)));

% pairwise correlation
for i = 1:N
    ind = [1:i-1 i+1:N];
    for j = 1:length(ind)
        temp = corrcoef(fn(i,:), fn(ind(j),:));
        cc_new(i,j) = temp(1,2);
        temp = corrcoef(f(i,:), f(ind(j),:));
        cc_old(i,j) = temp(1,2);  
    end
end
disp(sprintf('cc = %5.4f', mean(cc_new(:))/mean(cc_old(:))));

% Sobolev metric
bin = mean(diff(t));
for i = 1:N
    dfn(i,:) = gradient(fn(i,:), bin);
    df(i,:) = gradient(f(i,:), bin);
end
sm = sum(sum((dfn - ones(N,1)*mean(dfn,1)).^2))/sum(sum((df - ones(N,1)*mean(df,1)).^2));
disp(sprintf('sm = %5.4f', sm));


% %%%%%%%%%%%%% analysis and plots based on the output %%%%%%%%%%%%%%%%%%%%
% % plot aligned Y_i(t) (with noises) comparing to the simulated X_i(t) (without noises) 
% figure(1)
% hold on
% for i=1:N
%     plot(T,Y(i,:),'-r')
% end
% xlabel('t')
% ylabel('Y_i(t)')
% title('simulated Y_i(t) with warping and noises')
% hold off
% 
% figure(2)
% hold on
% for i=1:size(aligned,1)
%     plot(T,aligned(i,:),'-r')
% end
% xlabel('t')
% ylabel('Y*_i(t)')
% title('aligned Y_i(t)')
% hold off
% % plot the cross-sectional mean of Y_i(t), cross-sectional mean of aligned Y_i(t) and the true mean 
% figure(3)
% hold on
% plot(T,mean(Y),'-r')
% plot(T,mean(aligned),'-b')
% legend('mean of Y_i','mean of aligned Y_i','true mean','location','best')
% xlabel('t')
% title('cross-sectional means')
% hold off
% % plot the estimated inverse warping functions comparing to the simulated ones
% figure(4)
% hold on
% for i=1:size(hinv,1)
%     plot(T,hinv(i,:),'-b')
% end
% hold off
% xlabel('t')
% ylabel('Y(t)')
% title('estimated inverse warping functions')
