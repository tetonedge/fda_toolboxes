% main code to test Ramsay's code

clear;

addpath('../fdaM');
% load toy_data
load('~/Documents/Research/SRVF_FDA/Data/data2.mat')
f = f(:,1:131);
[M,N] = size(f);
for r = 1:25
    for i = 1:N
        f(2:(M-1),i) = (f(1:(M-2),i)+2*f(2:(M-1),i) + f(3:M,i))/4;
    end
end

t0 = t;

t = (t-t(1))/(t(end)-t(1));

ybasis = create_bspline_basis([0,1],71);

%  define the basis for the log-derivative of the warping function
nwbasis = 12;
wbasis  = create_bspline_basis([0,1],11);
Wfd0Par = fdPar(wbasis);

ff = f;
k = 0;
tic;
for i = 1:3
    
    %  target function
    k = k+1;
    mf(:,k) = mean(ff, 2);
    y0fd = smooth_basis(t, mf(:,k), ybasis);
    
    %  function to be registered has twice the amplitude
    yfd = smooth_basis(t, ff, ybasis);
    
    %  registration with the minimum eigenvalue criterion
    [regfd, warpfd, Wfd, shift, Fstr, iternum] = ...
        register_fd_new(y0fd, yfd, Wfd0Par, 0, 2,1e-12);
    ff = eval_fd(t, regfd);
    gam{i} = eval_fd(t,warpfd);
    if norm(mf(:,k) - mean(ff,2))< 1e-3
        break;
    end
end
toc,

gam = gam{1};

f0 = f;
fn = ff;
N = size(f0,2);

fmean = mean(fn,2);

fgam = zeros(size(fn));
for i = 1:size(fn,2)
    fgam(:,i) = interp1(t, fmean, (t(end)-t(1)).*gam(:,i) + t(1),'linear',0);
end

figure(1);
set(gcf, 'position', [200 500 250 200]);
plot(t0, fn);
%axis([t0(1) t0(end) -1.2 1.2]);  % wave data
%axis([t0(1) t0(end) 0 1.2]);  % extreme data
% axis([t0(1) t0(end) 0 9]);  % toy data
%axis([t0(1) t0(end) 0 1.5]);  % signa_data, nueral data, simu data
%axis([t0(1) t0(end) -5 35]);  % growth male data
%axis([t0(1) t0(end) -2.5 3]);  % gene data

% level of syncrhonization
for i = 1:N
    ind = [1:i-1 i+1:N];
    sync(i) = sum((fn(:,i)-mean(fn(:,ind),2)).^2)/sum((f0(:,i)-mean(f0(:,ind),2)).^2);
end
disp(sprintf('sync = %5.4f', mean(sync)));

% pairwise correlation
for i = 1:N
    ind = [1:i-1 i+1:N];
    for j = 1:length(ind)
        temp = corrcoef(fn(:,i), fn(:,ind(j)));
        cc_new(i,j) = temp(1,2);
        temp = corrcoef(f0(:,i), f0(:,ind(j)));
        cc_old(i,j) = temp(1,2);
    end
end
disp(sprintf('cc = %5.4f', mean(cc_new(:))/mean(cc_old(:))));

% Sobolev metric
bin = mean(diff(t));
for i = 1:N
    dfn(:,i) = gradient(fn(:,i), bin);
    df0(:,i) = gradient(f0(:,i), bin);
end
sm = sum(sum((dfn - mean(dfn,2)*ones(1,N)).^2))/sum(sum((df0 - mean(df0,2)*ones(1,N)).^2));
disp(sprintf('sm = %5.4f', sm));

