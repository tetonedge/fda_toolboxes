function [wtvec, onewt] = wtcheck(n, wtvec)
%WTCHECK checks an input weight vector.  
%  If the weight vector is not empty, it checked for dimension, length and 
%  positivity.  In this case output ONEWT has value 0.
%  If the weight vector is empty, it is set up as a vector of ones of
%  the appropriate length.  In this case output ONEWT has value 1.

%  Last modified 23 July 2010 by Jim Ramsay

if ~isempty(wtvec)
    sizew = size(wtvec);
    if (length(sizew) > 1 && sizew(1) > 1 && sizew(2) > 1) || ...
            length(sizew) > 2
        error ('WTVEC must be a vector.');
    end
    if length(sizew) == 2 && sizew(1) == 1
        wtvec = wtvec';
    end
    if length(wtvec) ~= n
        error('WTVEC of wrong length');
    end
    if min(wtvec) <= 0
        error('All values of WTVEC must be positive.');
    end
    onewt = 0;
else
    wtvec = ones(n,1);
    onewt = 1;
end
