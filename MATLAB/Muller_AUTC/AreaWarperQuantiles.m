function [aligned,h] = AreaWarperQuantiles(Data)
%This code should not be used for extensive testing, it is just a _rough_
%implementation of the method outlined in : Functional density 
%synchronization (2011) by Zhen Zhang and Hans-Georg Müller and it may
%have mistakes

%Data : Data to normalize assumed in a fixed equi-spaced grid
[N, M] = size(Data); %N subjects, M readings

T = linspace(0,1,M); %Arbitary time-scale for the data

%Normalize the Area under the curve of the data
DataAreaNorm = zeros(N,M); Original_Areas = zeros(N,1);
for z = 1:N
    Original_Areas(z) =  trapz( linspace(0,1,M) ,Data(z,:));
    DataAreaNorm(z,:) = Data(z,:) /Original_Areas(z) ;
end

%Calculate the CDF-like estimates for the normalized area data
CDFs = zeros(N,M);
for u = 1: N
    for t =2:M
        CDFs(u,t)= trapz( linspace(0,t/M,t) ,DataAreaNorm(u,1:t));
        if (CDFs(u,t) < CDFs(u,t-1))
            CDFs(u,t) = CDFs(u,t-1); %to account of numerical inconsistencies during integration
        end
        if (1 < CDFs(u,t))
            CDFs(u,t) = 1; %to account of numerical inconsistencies during integration
        end      
    end
end

%Calculate the Quantile functions as the inverse of the CDF-like estimates
QUAs = zeros(N,M);
for u = 1: N
    QUAs(u,:) = interp1(  CDFs(u,:) +  linspace(0,10^-9,M) , linspace(0,1,M), linspace(0,1,M)   );                   
    QUAs(u,M) = 1; %Just to make sure we get one sometimes we get NaN
end

%Take the Quantile mean as the "true" mean synchronization function
TrueMean  = mean(QUAs);

%Find differences from each individual sync. function to the "true" one.
%We can call the Quantile Function.  Synchronization function
Diffs = repmat(TrueMean, N,1)- QUAs; 

%Define new timescale to interpolate upon (inverse time / quantile domain)
hinvs = Diffs + repmat( T, N,1);

%%%%%%%%%%%%%%%%%
%Most probably here you need to do some smoothing / penalize roughness
%%%%%%%%%%%%%%%%%

%Define timescale to interpolate upon 
h = zeros(N,M);
for u = 1: N
    h(u,:) = interp1(  hinvs(u,:), T, T );
end

%Calculate the warped data in the Area Normalized domain 
%(these don't integrate to 1 now).
WarpedData_AreaNormalized = Data; 
for u=1:N
    WarpedData_AreaNormalized(u,:) = interp1(T, DataAreaNorm(u,:), h(u,:));
end 

%Fix the data scale by multiplying them with what you divided above
WarpedData_AreaNormalized(:,M) = DataAreaNorm(:,M); %fix possible problem with the last entry
aligned = WarpedData_AreaNormalized;
for z = 1:N
   aligned(z,:) = WarpedData_AreaNormalized(z,:) * (Original_Areas(z) /1);
end

