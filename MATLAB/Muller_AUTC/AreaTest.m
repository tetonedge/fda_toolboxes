clear; clc;
%Make a show case with a dataset of size N and length M:
N = 20; M = 300;
%1. Gaussian bumps
S0 = zeros(N,M);

MAX= 3*3;
T = linspace(0,MAX,M);

for i = 1:N
    u = randi(150,1,1)+1;  %random displacement from the start of the axis for the S0    
    S0(i,u:(u+149)) = hist(randn(10^5,1),150) .* ( 2 + rand() ); %make noisy gaussian bump
end

figure(1); 
title('The Gaussian Bumps');
%Things work rather nicely, probably cause they 
%actually look like legitimate prob. densities.
[y0,h0] =AreaWarperQuantiles ( S0 );

subplot(3,1,1);
plot(T,S0);xlabel('T'); ylabel('f(t)'); xlim([0 MAX]); title('Unwarped Data')
subplot(3,1,2);
plot(T,y0);xlabel('T'); ylabel('f(t)'); xlim([0 MAX]);title('Warped Data')
subplot(3,1,3);
plot(T,h0); xlabel('T'); ylabel('f(t)'); xlim([0 MAX]);title('Warping Funct.')
