%  test extended registration function, register_fd_new

t = linspace(0,1,101);

y0 = sin(2*pi*t);

w = 0.5;

h = (1 - exp(-w.*t))./(1 - exp(-w));

plot(t,h, '-', t, t, '--')

ybasis = create_bspline_basis([0,1],51);

%  target function

y0fd = smooth_basis(t, y0, ybasis);

%  function to be registered has twice the amplitude

yfd = smooth_basis(h, 2*y0, ybasis);

plot(yfd)
line(2.*y0fd)

%  define the basis for the log-derivative of the warping function

nwbasis = 11;
wbasis  = create_bspline_basis([0,1],5);
Wfd0Par = fdPar(wbasis);

%  registration with the unweighted least squares criterion

[regfd, warpfd, Wfd, shift, Fstr, iternum] = ...
              register_fd_new(y0fd, yfd, Wfd0Par, 0, 1);

figure(1)
subplot(2,1,1)
plot(regfd)
line(y0fd)
xlabel('')
title('Unweighted least squares')

subplot(2,1,2)
plot(warpfd)
hold on
plot(t, h, 'g--')
hold off
xlabel('')
legend('Estimated warping', 'True warping', 'location', 'NorthWest')
title('Unweighted least squares')

%  registration with the minimum eigenvalue criterion

[regfd, warpfd, Wfd, shift, Fstr, iternum] = ...
              register_fd_new(y0fd, yfd, Wfd0Par, 0, 2);
          
figure(2)
subplot(2,1,1)
plot(regfd)
line(y0fd)
xlabel('')
title('Second eigenvalue criterion')

subplot(2,1,2)
plot(warpfd)
hold on
plot(t, h, 'g--')
hold off
xlabel('')
legend('Estimated warping', 'True warping', 'location', 'NorthWest')
title('Second eigenvalue criterion')

%  registration with Fisher-Rao metric

[regfd, warpfd, Wfd, shift, Fstr, iternum] = ...
              register_fd_new(y0fd, yfd, Wfd0Par, 0, 3);
          
figure(3)
subplot(2,1,1)
plot(regfd)
line(y0fd)
xlabel('')
title('Fisher-Rao metric')

subplot(2,1,2)
plot(warpfd)
hold on
plot(t, h, 'g--')
hold off
xlabel('')
legend('Estimated warping', 'True warping', 'location', 'NorthWest')
title('Fisher-Rao metric')

%  set up fd object for root-derivative of target y0

Dy0fd = deriv(y0fd);
plot(Dy0fd)
Dy0vec = eval_fd(t, Dy0fd);
modDy0vec = abs(Dy0vec);
rtDy0vec = Dy0vec./sqrt(modDy0vec);
rtDy0fd = smooth_basis(t, rtDy0vec, ybasis);
plotfit_fd(rtDy0vec, t, rtDy0fd)

%  set up fd object for root-derivative of function y to be registered

Dyfd = deriv(yfd);
plot(Dyfd)
Dyvec = eval_fd(t, Dyfd);
modDyvec = abs(Dyvec);
rtDyvec = Dyvec./sqrt(modDyvec);
rtDyfd = smooth_basis(t, rtDyvec, ybasis);
plotfit_fd(rtDyvec, t, rtDyfd)

%  do the registration in the root_Dh metric

[regfd, warpfd, Wfd, shift, Fstr, iternum] = ...
              register_fd_new(rtDy0fd, rtDyfd, Wfd0Par, 0, 3);

figure(4)
subplot(2,1,1)
plot(regfd)
line(rtDy0fd)
xlabel('')
title('Fisher-Rao metric')

subplot(2,1,2)
plot(warpfd)
hold on
plot(t, h, 'g--')
hold off
xlabel('')
legend('Estimated warping', 'True warping', 'location', 'NorthWest')
title('Fisher-Rao metric and signed-root objects')


