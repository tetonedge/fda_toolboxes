% estimation of synchronization

clear; close all;
% 
% load fn_toy_data.txt;
% load ../software/toy_data;
% fn = fn_toy_data;

% load fn_simu_data.txt;
% load ../software/simu_data;
% fn = fn_simu_data;

% load fn_CurvReg3ieg11.txt;
% load ../software/CurvReg3ieg11ToyDataset;
% fn = fn_CurvReg3ieg11;

% load fn_wave_data.txt;
% load ../software/wave_data;
% fn = fn_wave_data;

% load fn_growth_male_vel.txt;
% load ../software/growth_male_vel;
% fn = fn_growth_male_vel;

% load fn_signa_data.txt;
% load ../software/signa_data;
% fn = fn_signa_data;

% load fn_neural_data.txt;
% load ../software/neural_data;
% fn = fn_neural_data;

load fn_gene_data.txt;
load ../software/gene_data;  t = t'; f = f(l==1,:)';
fn = fn_gene_data;


f0 = f;
N = size(f0, 2);

% level of syncrhonization
for i = 1:N
    ind = [1:i-1 i+1:N];
    sync(i) = sum((fn(:,i)-mean(fn(:,ind),2)).^2)/sum((f0(:,i)-mean(f0(:,ind),2)).^2);
end
disp(sprintf('sync = %5.4f', mean(sync)));

% pairwise correlation
for i = 1:N
    ind = [1:i-1 i+1:N];
    for j = 1:length(ind)
        temp = corrcoef(fn(:,i), fn(:,ind(j)));
        cc_new(i,j) = temp(1,2);
        temp = corrcoef(f0(:,i), f0(:,ind(j)));
        cc_old(i,j) = temp(1,2);       
    end
end
disp(sprintf('cc = %5.4f', mean(cc_new(:))/mean(cc_old(:))));

% Sobolev metric
bin = mean(diff(t));
for i = 1:N
    dfn(:,i) = gradient(fn(:,i), bin);
    df0(:,i) = gradient(f0(:,i), bin);
end
sm = sum(sum((dfn - mean(dfn,2)*ones(1,N)).^2))/sum(sum((df0 - mean(df0,2)*ones(1,N)).^2));
disp(sprintf('sm = %5.4f', sm));


ff(:,:,1) = f;
ff(:,:,2) = fn;

for k = 1:2
    figure(k);
    set(gcf, 'position', [200 500 250 200]);
    plot(t, ff(:,:,k));  
    %axis([t(1) t(end) -1.2 1.2]);  % wave data
    %axis([t(1) t(end) 0 1.2]);  % extreme data
    %axis([t(1) t(end) 0 9]);  % toy data
    %axis([t(1) t(end) 0 1.5]);  % signa_data, nueral data, simu data
    %axis([t(1) t(end) -5 35]);  % growth male data    
    axis([t(1) t(end) -2.5 3]);  % gene data
end

