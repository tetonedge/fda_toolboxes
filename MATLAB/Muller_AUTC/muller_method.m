% Test on Muller's method

clear; close all;

%load ../software/toy_data;
%load ../software/simu_data;
%load ../software/CurvReg3ieg11ToyDataSet;
%load ../software/wave_data;
%load ../software/growth_male_vel;
%load ../software/growth_female_vel;
load ../software/signa_data;
%load ../software/neural_data;
%load ../software/gene_data;  t = t'; f = f(l==1,:)';

t0 = t;
t = (t'-t(1))/(t(end)-t(1));
f = f';

% figure(1);
% plot(t, f);
% title('Original data', 'fontsize', 14);

tic;

[M, N] = size(f);

p = 1;
for k = 1:M
    phi(k,:) = (cumtrapz(t, abs(f(k,:)).^p)/trapz(t, abs(f(k,:)).^p)).^(1/p);
    phi(k,:) = phi(k,:)+1e-12*(1:N);
    phi(k,:) = (phi(k,:)-min(phi(k,:)))/(max(phi(k,:)) - min(phi(k,:)));
    phi_inv(k,:) = interp1(phi(k,:), t, t);
end

% figure(2);
% plot(t, phi, 'b', t, phi_inv, 'r');
% title('Warping function', 'fontsize', 14);
% axis equal;

for k = 1:M
    fn(k,:) = interp1(t, f(k,:), phi_inv(k,:));
end

toc,

figure(1);
set(gcf, 'position', [200 500 250 200]);
plot(t0, fn);
%axis([t0(1) t0(end) -1.2 1.2]);  % wave data
%axis([t0(1) t0(end) 0 1.2]);  % extreme data
%axis([t0(1) t0(end) 0 9]);  % toy data
%axis([t0(1) t0(end) 0 1.5]);  % signa_data, nueral data, simu data
%axis([t0(1) t0(end) -5 35]);  % growth male data
axis([t0(1) t0(end) -2.5 3]);  % gene data

% measure the level of synchronization (James, 2011)
for i = 1:M
    ind = [1:i-1 i+1:M];
    sync(i) = sum((fn(i,:)-mean(fn(ind,:),1)).^2)/sum((f(i,:)-mean(f(ind,:),1)).^2);
end
disp(sprintf('sync = %5.4f', mean(sync)));

% pairwise correlation
for i = 1:M
    ind = [1:i-1 i+1:M];
    for j = 1:length(ind)
        temp = corrcoef(fn(i,:), fn(ind(j),:));
        cc_new(i,j) = temp(1,2);
        temp = corrcoef(f(i,:), f(ind(j),:));
        cc_old(i,j) = temp(1,2);  
    end
end
disp(sprintf('cc = %5.4f', mean(cc_new(:))/mean(cc_old(:))));

% Sobolev metric
bin = mean(diff(t));
for i = 1:M
    dfn(i,:) = gradient(fn(i,:), bin);
    df(i,:) = gradient(f(i,:), bin);
end
sm = sum(sum((dfn - ones(M,1)*mean(dfn,1)).^2))/sum(sum((df - ones(M,1)*mean(df,1)).^2));
disp(sprintf('sm = %5.4f', sm));
